
# Storing/retrieving data with redux
## Brainstorming

- Idea of pages -- either a page is loaded or it isn't -- we don't need to have the whole thing in memory
    - Could be by app as well as by date -- possibly one before the other.  Then it's obvious if we need to hit the database or not.
    - Pages stored by weeks most likely -- we probably rarely want more than a week or two of span data

### local storage things to save

- tasks
- spans
- timer start date and running or not

# Todo

x Google auth -- don't attempt to authenticate (creating popup) unless user requests it.
x Span persist -- only on user action -- never due to timer expiring.
 Span edit
 Do something on notification clicked
x Show "break" as "next up" in timer
