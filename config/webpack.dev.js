const common = require("./webpack.common");
const path = require("path");

module.exports = {
    ...common,
    devtool: 'eval-source-map',
    devServer: {
        contentBase: path.join(__dirname, "../", "dist"),
        publicPath: "/",
        compress: true,
        port: 8000,
        hot: false,
        host: "0.0.0.0",
        disableHostCheck: true,
        index: "index.html",
        watchOptions: {
            poll: 500,
            ignore: 'node_modules'
        },
        setup(app) {
            app.get("/code", (req, res) => {
                var header = req.header("Authorization");
                if (header) {
                    res.send(header.split(" ")[1]);
                } else {
                    res.status(404).end();
                }
            });
        },
    },
};
