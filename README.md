## FocusTask - A time tracking task management app
### Objective
Combine work inventory, planning, execution, and recording tools into one, that has a simple and easy-to-use interface that requires minimal overhead to operate.

### Current features

- Installable as a PWA
- Usable offline
- Does not require you to use any cloud services
- Has a full-screen mode
- Allows choosing from several themes
- Connects to Google Drive so that it can persist its data there (uses your drive space, but in an isolated area specifically for this app).  This allows for using the app from different devices.
- Recognizes tasks written in the [todo.txt](http://todotxt.org/) format, so that task metadata can all be entered without leaving the keyboard or even the line of the task.
- Automatically summarizes time spent on tasks based on how they have been tagged
- Integrated Pomodoro timer for practicing the pomodoro technique
- Uses [Notification Triggers](https://web.dev/notification-triggers/) for pomodoro alarms when the app is in the background.  This is an experimental Chrome feature only at the moment, but this does prevent the need for push notifications, which would require the use of cloud services and reliance on the internet for this.  It has fallbacks to simply use normal notifications, or to even use javascript alerts if those have not been allowed yet.
- Simply type and hit a button to start a new (possibly unexpected) task
- Switch tasks with the click of a button.

### Planned features

- More advanced planning and tasks, so that you can quickly see which tasks likely need the most attention (i.e., due to deadlines and estimated effort, etc.)
- Personal Kanban for tasks

### Contributing

As the original author of this, it was kind of my first forray into building anything real with React.  Consequently, I know it probably has some serious flaws.

Any contributions welcome!  Unless I have a serious concern with something, I want to accept any pull request you submit, and respond to any issue you submit.

### Other work needing to be done

- Testing --  (This is embarrasing) no tests yet!  This is bad!  It's not an excuse, but I was just playing around and not working on this project too seriously, and that's why.