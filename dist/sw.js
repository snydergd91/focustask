
importScripts('https://storage.googleapis.com/workbox-cdn/releases/3.6.3/workbox-sw.js');

workbox.routing.registerRoute(
  new RegExp('\.js$'),
  workbox.strategies.networkFirst({
    cacheName: 'javascript-cache',
  })
);

workbox.routing.registerRoute(
  new RegExp('(\.html|/)$'),
  workbox.strategies.networkFirst({
    cacheName: 'html-cache',
  })
);
workbox.routing.registerRoute(
  ({url}) => url.pathname.match(new RegExp("\.(js|css|woff2|ttf|svg)$")),
  workbox.strategies.staleWhileRevalidate(),
);
workbox.routing.registerRoute(
    'https://couchdb.george.gdsnyder.info/_session',
    workbox.strategies.networkFirst()
);
workbox.routing.registerRoute(
    new RegExp('\.webmanifest$'),
    workbox.strategies.staleWhileRevalidate(),
);