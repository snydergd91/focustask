import React from "react";
import { connect } from "react-redux";
import {
  useRouteMatch,
  useLocation,
  useParams,
  useHistory,
  Route,
  Link,
} from "react-router-dom";
import drive from "../util/drive";
import * as filePickerActions from "../ducks/filepicker";
import * as appActions from "../ducks/app";

export function Item({ onError }) {
  const [fileContent, setFileContent] = React.useState(false);
  const [fileInfo, setFileInfo] = React.useState(false);
  const { fileId } = useParams();
  const history = useHistory();
  const match = useRouteMatch();

  React.useEffect(() => {
    drive.ready
      .then(() => drive.getContentById(fileId))
      .then(setFileContent)
      .catch(onError);
    drive.ready
      .then(() => drive.getFileById(fileId))
      .then(setFileInfo)
      .catch(onError);
  }, []);

  return (
    <div>
      {fileContent !== false ? (
        <>
          <h5>
            {fileInfo.result.name + " "}
            <button
              type="button"
              className="btn btn-sm btn-primary"
              onClick={() => history.push(match.path.replace(/[^\/]+$/, ''))}
            >
              Pick another file
            </button>
          </h5>
          <pre>
            <code>{fileContent}</code>
          </pre>
        </>
      ) : (
        "Loading..."
      )}
    </div>
  );
}
export function DrivePicker({ items = [], onDisplayItems, onError } = {}) {
  const match = useRouteMatch();
  const location = useLocation();

  React.useEffect(() => {
    if (location.pathname == match.path) {
      onDisplayItems();
    }
  }, [location]);

  return (
    <div className="container">
      <h2>Stored Files</h2>
      <Route exact path={match.path}>
        <div>
          Item Count: {items.length}
          <button
            type="button"
            className="btn btn-link"
            onClick={() => {
              onDisplayItems();
            }}
          >
            Refresh
          </button>
        </div>
        <ul>
          {items.map((file) => (
            <li key={file.id}>
              <Link to={`${match.path}/${file.id}`}>{file.name}</Link>
            </li>
          ))}
        </ul>
      </Route>
      <Route path={`${match.path}/:fileId`}>
        <Item {...{ onError }} />
      </Route>
    </div>
  );
}

function mapStateToProps(state, ownProps) {
  return {
    items: state.filepicker.items,
    spans: state.timer.spans,
    ...ownProps,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    onDisplayItems: () => {
      drive.ready
        .then(() => drive.listFiles())
        .then((files) => dispatch(filePickerActions.setItems(files)))
        .catch((error) => dispatch(appActions.triggerError(error)));
    },
    onError: (error) => dispatch(appActions.triggerError(error)),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(DrivePicker);
