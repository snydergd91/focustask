import React from "react";

export default class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true, error };
  }

  componentDidCatch(error, errorInfo) {
    // You can also log the error to an error reporting service
    console.log(this.props.extraData, error, errorInfo);
  }

  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return (
        <pre className="alert alert-danger">
          {JSON.stringify(this.state.error, null, " ") + this.state.error.toString()} {JSON.stringify(this.props.extraData, null, " ")}
        </pre>
      );
    }

    return this.props.children;
  }
}
