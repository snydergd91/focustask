import React from "react";
import * as timeUtils from "../util/time";
import dateFormat from "dateformat";

const weekdays = [
  "Sunday",
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
];

function Calendar({
  selectedDate,
  dateSelected = () => {},
  className,
  actions,
}) {
  const selectedDateOnlyDate = new Date(
    selectedDate.getFullYear(),
    selectedDate.getMonth(),
    selectedDate.getDate()
  );
  const [monthStart, setMonthStart] = React.useState(
    timeUtils.fromParts({ ...timeUtils.parts(selectedDateOnlyDate), day: 1 })
  );
  function addMonth(number) {
    setMonthStart(
      timeUtils.fromParts({
        ...timeUtils.parts(monthStart),
        month: monthStart.getMonth() + number + 1,
      })
    );
  }
  const offset = monthStart.getDay() - 1;
  const daysInMonth = new Date(
    monthStart.getFullYear(),
    monthStart.getMonth() + 1,
    0
  ).getDate();
  const rows = Math.ceil((offset + daysInMonth + 1) / 7);
  return (
    <>
      <table className={`table table-bordered ${className}`}>
        <thead>
          <tr>
            <th className="p-0" style={{ textAlign: "center" }} colSpan="7">
              <h5>
                <button
                  className="btn btn-link"
                  onClick={() => addMonth(-1)}
                  title="Previous Month"
                >
                  <span className="sr-only">Previous Month</span>
                  <i className="fa fa-angle-left" />
                </button>
                {dateFormat(monthStart, "mmmm, yyyy")}
                <button
                  className="btn btn-link"
                  onClick={() => addMonth(1)}
                  title="Next Month"
                >
                  <span className="sr-only">Next Month</span>
                  <i className="fa fa-angle-right" />
                </button>
                {actions || false}
              </h5>
            </th>
          </tr>
          <tr>
            {weekdays.map((day) => (
              <th className="p-0 text-center" key={day}>
                {day[0]}
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {[...Array(rows)].map((_, row) => {
            const weekStartDayOfMonth = -offset + row * 7;
            return (
              <tr key={row}>
                {[...Array(7)].map((_, col) => {
                  const dayOfMonth = weekStartDayOfMonth + col;
                  const inRange = dayOfMonth > 0 && dayOfMonth <= daysInMonth;
                  const date = timeUtils.fromParts({
                    ...timeUtils.parts(monthStart),
                    day: dayOfMonth,
                  });
                  return (
                    <td key={dayOfMonth} className="p-0">
                      {inRange && (
                        <button
                          type="button"
                          className={`btn btn-block btn-${
                            inRange &&
                            date.getTime() === selectedDateOnlyDate.getTime()
                              ? "primary"
                              : "default"
                          } btn-small`}
                          onClick={() => {
                            const newParts = timeUtils.parts(date);
                            dateSelected(
                              timeUtils.fromParts({
                                ...timeUtils.parts(selectedDate),
                                year: newParts.year,
                                month: newParts.month,
                                day: newParts.day,
                              })
                            );
                          }}
                        >
                          {dayOfMonth}
                        </button>
                      )}
                    </td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}
export default Calendar;
