import React from "react";
import { useHistory } from "react-router-dom";
import { connect } from "react-redux";
import { util as taskUtil } from "../ducks/tasks";
import * as timerActions from "../ducks/timer";
import * as timeUtils from "../util/time";
import DateEdit from "./DateEdit";
import TextEdit from "./TextEdit";
import TimeEdit from "./TimeEdit";

function SpanView({ span, onDelete, onUpdate, tasks }) {
  const history = useHistory();

  return (
    <div>
      <div>
        <div className="row d-flex">
          <h5 className="flex-grow-1">
            <TextEdit
              value={span.task}
              onChange={(newVal) =>
                onUpdate({
                  ...(span.original || span),
                  task: newVal,
                })
              }
            />
          </h5>
          <div>
            {span.taskId in tasks &&
              span.task !== taskUtil.taskToLine(tasks[span.taskId]) && (
                <button
                  className="btn btn-default"
                  title="Reconcile historic task to current task"
                  onClick={() =>
                    onUpdate({
                      ...(span.original || span),
                      task: taskUtil.taskToLine(tasks[span.taskId]),
                    })
                  }
                >
                  <span className="sr-only">
                    Reconcile historic task to current task
                  </span>
                  <i className="fa fa-balance-scale" />
                </button>
              )}
            {span.taskId in tasks && (
              <button
                className="btn btn-default"
                onClick={() => history.push(`/task/${span.taskId}`)}
                title="Go to task"
              >
                <span className="sr-only">Go to task</span>
                <i className="fa fa-tasks" />
              </button>
            )}
            <button
              className="btn btn-danger"
              onClick={() => onDelete(span)}
              title="Delete"
            >
              <span className="sr-only">Delete</span>
              <i className="fa fa-close" />
            </button>
          </div>
        </div>
        <div>
          <span className="badge badge-info badge-pill">
            <b>Duration: </b>
            {timeUtils.formatSecondsAsTime(span.duration / 1000)}
          </span>
          {span.taskId in tasks && (
            <span className="badge badge-info badge-pill">
              <b>Task: </b>
              {tasks[span.taskId].title.length > 0 ? (
                <select
                  value={span.taskId}
                  onChange={(e) => {
                    onUpdate({ ...span, taskId: e.target.value });
                  }}
                >
                  {Object.values(tasks).map((task) => (
                    <option key={task.id} value={task.id}>
                      {task.title}
                    </option>
                  ))}
                </select>
              ) : (
                "<deleted>"
              )}
            </span>
          )}
        </div>
        <div>
          <b>Start: </b>
          <DateEdit
            value={span.start}
            onUpdate={(start) =>
              onUpdate({
                ...span,
                start,
              })
            }
          />{" "}
          <TimeEdit
            value={span.start}
            onUpdate={(start) =>
              onUpdate({
                ...span,
                start,
              })
            }
          />
        </div>
        <div>
          <b>End: </b>
          <DateEdit
            value={span.end}
            onUpdate={(end) =>
              onUpdate({
                ...span,
                end,
                duration: end.getTime() - span.start.getTime(),
              })
            }
          />{" "}
          <TimeEdit
            value={span.end}
            onUpdate={(end) =>
              onUpdate({
                ...span,
                end,
                duration: end.getTime() - span.start.getTime(),
              })
            }
          />
        </div>
      </div>
    </div>
  );
}

export default connect(
  function (state, ownProps) {
    return { tasks: state.tasks.tasks };
  },
  function (dispatch) {
    return {
      onDelete: (span) => dispatch(timerActions.deleteSpan(span.id)),
      onUpdate: (span) => dispatch(timerActions.updateSpan(span)),
    };
  }
)(SpanView);
