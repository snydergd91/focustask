import React from "react";

export default function TextEdit({ value, onChange }) {
  const [myValue, setMyValue] = React.useState(value);
  const [edit, setEdit] = React.useState(false);
  const entry = React.useRef();

  React.useEffect(() => {
    if (edit) {
      entry.current && entry.current.focus();
    }
  }, [edit]);

  function commit() {
    if (myValue !== value) {
      onChange(myValue);
    }
    setEdit(false);
  }
  function startEdit() {
    setMyValue(value);
    setEdit(true);
  }
  return (
    <span>
      {edit ? (
        <div className="input-group">
          <input
            className="form-control"
            type="text"
            ref={entry}
            onBlur={commit}
            onKeyUp={(e) => {
              e.key == "Enter" && commit();
              e.key == "Escape" && setEdit(false);
            }}
            onChange={(e) => setMyValue(e.target.value)}
            value={myValue}
          />
          <div className="input-group-append">
            <button
              className="btn btn-secondary"
              title="Cancel"
              onClick={() => {
                setEdit(false);
              }}
            >
              <span className="sr-only">Cancel</span>
              <i className="fa fa-close" />
            </button>
            <button className="btn btn-success" title="OK" onClick={commit}>
              <span className="sr-only">OK</span>
              <i className="fa fa-check" />
            </button>
          </div>
        </div>
      ) : (
        <div onClick={startEdit}>
          <button className="btn btn-default" title="Edit" onClick={startEdit}>
            <span className="sr-only">Edit</span>
            <i className="fa fa-pencil" />
          </button>
          {value}
        </div>
      )}
    </span>
  );
}
