import dateFormat from "dateformat";
import React from "react";
import * as timeUtils from "../util/time";

const updateTimeState = (time, part, setTime) => (e) => {
  setTime({ ...time, [part]: parseInt(e.target.value) || 0 });
};
export default function TimeEdit({ value, onUpdate }) {
  const [edit, setEdit] = React.useState(false);
  const [time, setTime] = React.useState(timeUtils.parts(value));

  return (
    <span style={{ display: "inline-table" }}>
      {edit ? (
        <>
          <div className="d-flex">
            <input
              type="number"
              max="23"
              min="0"
              size="2"
              value={time.hour || ""}
              onChange={updateTimeState(time, "hour", setTime)}
            />
            :
            <input
              type="number"
              max="59"
              min="0"
              size="2"
              value={time.minute || ""}
              onChange={updateTimeState(time, "minute", setTime)}
            />
            :
            <input
              type="number"
              max="59"
              min="0"
              size="2"
              value={time.second || ""}
              onChange={updateTimeState(time, "second", setTime)}
            />
            <div>
              <button
                className="btn btn-success"
                onClick={() => {
                  onUpdate(timeUtils.fromParts(time));
                  setEdit(false);
                }}
                title="Done"
              >
                <span className="sr-only">Done</span>
                <i className="fa fa-check" />
              </button>
              <button
                className="btn btn-secondary"
                onClick={() => setEdit(false)}
                title="Cancel"
              >
                <span className="sr-only">Cancel</span>
                <i className="fa fa-close" />
              </button>
            </div>{" "}
          </div>
        </>
      ) : (
        <>
          <button
            className="btn btn-default"
            onClick={() => {
              setTime(timeUtils.parts(value));
              setEdit(true);
            }}
            title="Edit"
          >
            <span className="sr-only">Edit</span>
            <i className="fa fa-pencil" />
          </button>
          {dateFormat(value, "H:MM:ss")}
        </>
      )}
    </span>
  );
}
