import React from "react";
import { connect } from "react-redux";
import { util as taskUtil } from "../ducks/tasks";
import * as timerActions from "../ducks/timer";
import * as timeUtil from "../util/time";
import ErrorBoundary from "./ErrorBoundary";
import RecordedTimeDay from "./RecordedTimeDay";

export function RecordedTime({ spans, loading = false, onDeleteSpan } = {}) {
  const spanData = {};
  for (const span of spans) {
    const start = new Date(span.end - span.duration);
    const startDateObj = new Date(
      start.getFullYear(),
      start.getMonth(),
      start.getDate()
    );
    const startDate = startDateObj.getTime();
    if (!(startDate in spanData)) {
      spanData[startDate] = {
        dateObj: startDateObj,
        spans: [],
      };
    }
    spanData[startDate].spans.push({
      ...span,
      start,
      end: new Date(span.end),
      original: span,
    });
  }

  for (const day of Object.values(spanData)) {
    day.spans.sort((a, b) => b.start.getTime() - a.start.getTime());
  }

  const tagTime = {};
  const singleTagTime = {};
  for (const span of spans) {
    const task = taskUtil.parseTask(span.task);
    const tags = task.contextTags
      .map((x) => "@" + x)
      .concat(task.projectTags.map((x) => "+" + x));
    for (const tag of tags) {
      if (!(tag in tagTime)) tagTime[tag] = 0;
      tagTime[tag] += span.duration;
    }
    const primaryTag = task.contextTags.length
      ? "@" + task.contextTags[0]
      : task.projectTags.length
      ? "+" + task.projectTags[0]
      : "other";
    if (!(primaryTag in singleTagTime)) singleTagTime[primaryTag] = 0;
    singleTagTime[primaryTag] += span.duration;
  }

  return (
    <div>
      {loading ? (
        "Loading..."
      ) : (
        <>
          <h4>Summary</h4>
          <h5>Time by primary tag</h5>
          <ul>
            {Object.entries(singleTagTime).map(([tag, time]) => (
              <li key={tag}>
                {tag}: {timeUtil.formatSecondsAsTime(time / 1000)}
              </li>
            ))}
          </ul>
          <h5>Time by tag</h5>
          <ul>
            {Object.entries(tagTime).map(([tag, time]) => (
              <li key={tag}>
                {tag}: {timeUtil.formatSecondsAsTime(time / 1000)}
              </li>
            ))}
          </ul>
          <h4>Entries</h4>
          {Object.values(spanData)
            .sort((a, b) => b.dateObj.getTime() - a.dateObj.getTime())
            .map((day) => (
              <ErrorBoundary extraData={day} key={day.dateObj.getTime()}>
                <RecordedTimeDay day={day} onDeleteSpan={onDeleteSpan} />
              </ErrorBoundary>
            ))}
        </>
      )}
    </div>
  );
}

function mapStateToProps(state) {
  return {
    spans: Object.values(state.timer.spans)
      .map(x => ({ ...x, end: new Date(x.end).getTime() }))
      .filter((x) => {
        const start = new Date(
          new Date(x.end).getTime() - x.duration
        ).getTime();
        return (
          !x.deleted &&
          start >= state.app.dateRange.start &&
          start <= state.app.dateRange.end+24*60*60*1000
        );
      }),
  };
}
function mapDispatchToProps(dispatch) {
  return {
    onDeleteSpan(span) {
      dispatch(timerActions.deleteSpan(span.id));
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(RecordedTime);
