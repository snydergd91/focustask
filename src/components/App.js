import dateFormat from "dateformat";
import React from "react";
import { connect } from "react-redux";
import { HashRouter as Router, Link, NavLink, Route, useHistory, useParams } from "react-router-dom";
import * as appActions from "../ducks/app";
import * as taskActions from "../ducks/tasks";
import showDialog from "../util/showDialogJquery";
import * as storage from "../util/storage";
import Calendar from "./Calendar";
import DrivePicker from "./DrivePicker";
import ErrorBoundary from "./ErrorBoundary";
import RecordedTime from "./RecordedTime.js";
import Settings from "./Settings";
import TaskItem from "./TaskItem";
import TaskList from "./TaskList.js";
import Timer from "./Timer.js";
import * as timeUtil from "../util/time";

function RangePicker({ start, end, setStart, setEnd }) {
    const [myStart, setMyStart] = React.useState(start);
    const [myEnd, setMyEnd] = React.useState(end);

    React.useEffect(() => {
        setStart(myStart);
    }, [myStart]);
    React.useEffect(() => {
        setEnd(myEnd);
    }, [myEnd]);

    return (
        <>
            <div style={
                { display: "inline-table" }
            }>
                <h6>Start</h6>
                <Calendar selectedDate={myStart}
                    dateSelected={setMyStart}
                    className="col-md-5" />
            </div>
            <div style={
                { display: "inline-table" }
            }>
                <h6>End</h6>
                <Calendar selectedDate={myEnd}
                    dateSelected={setMyEnd}
                    className="col-md-5" />
            </div>
        </>
    );
}


export function App({
    username,
    error,
    authorizationNeeded,
    drive,
    online,
    synced,
    tasks,
    dateRange,
    setDateRangeStart,
    setDateRangeEnd
}) {
    const history = useHistory();

    function chooseDateRange() {
        showDialog({
            title: "Choose an active date range",
            body: (
                <RangePicker start={
                    new Date(dateRange.start)
                }
                    end={
                        new Date(dateRange.end)
                    }
                    setStart={setDateRangeStart}
                    setEnd={setDateRangeEnd} />
            )
        });
    }
    const [isFullscreen, setIsFullscreen] = React.useState(document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement);
    React.useEffect(() => {
        const eventCallbackName = ["onfullscreenchange", "onwebkitfullscreenchange", "onmsfullscreenchange", "onmozfullscreenchange"].filter(x => x in document)[0];
        document[eventCallbackName] = () => {
            setIsFullscreen(document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement);
        }
    }, [setIsFullscreen]);
    const goFullscreen = () => {
        var root = document.documentElement;
        return (root.requestFullscreen || root.webkitRequestFullscreen || root.mozRequestFullScreen || root.msRequestFullscreen).call(document.documentElement);
    };
    const stopFullscreen = () => {
        return (document.exitFullscreen || document.webkitExitFullscreen || document.mozCancelFullScreen || document.msExitFullscreen).call(document);
    };

    return (
        <Router>
            <div className="container">
                {
                    error && (
                        <pre className="alert alert-danger">
                            {JSON.stringify(error, null, " ") + error.toString()}
                        </pre>)
                }
                <div className="my-3">
                    <Timer />
                </div>
                <div className="my-0 pull-right">
                    <button className="btn btn-default"
                        onClick={chooseDateRange}
                        title={
                            `Choose Date Range (current: ${
                            dateFormat(dateRange.start, "m-d")
                            } to ${
                            dateFormat(dateRange.end, "m-d")
                            })`
                        }>
                        <span className="sr-only">Choose Date Range</span>
                        <i className="fa fa-calendar" />
                    </button>
                    <button className="btn btn-default"
                        onClick={(e) => {
                            isFullscreen ? stopFullscreen() : goFullscreen();
                        }}
                        title="Toggle Fullscreen">
                        <span className="sr-only">Toggle Fullscreen</span>
                        <i className={`fa fa-${isFullscreen ? 'compress' : 'expand'}`} />
                    </button>
                </div>
                <ul className="nav nav-tabs clearfix">
                    {
                        [
                            [
                                "/", "Todo"
                            ],
                            [
                                "/recordedTime", "Analysis"
                            ],
                            [
                                "/settings", (
                                    <>
                                        <span className="sr-only">Settings</span><i className="fa fa-gear" /></>
                                )
                            ],
                            [
                                "/help", (
                                    <><span className="sr-only">Help</span><i className="fa fa-question-circle" /></>
                                )
                            ]
                        ].map(([path, text]) => (
                            <li className="nav-item"
                                key={path}>
                                <NavLink to={path}
                                    className="nav-link"
                                    activeClassName="active"
                                    exact={true}>
                                    {text} </NavLink>
                            </li>
                        ))
                    } </ul>
                <Route exact path="/">
                    <ErrorBoundary>
                        <TaskList />
                    </ErrorBoundary>
                </Route>
                <Route path="/recordedTime">
                    <ErrorBoundary>
                        <RecordedTime />
                    </ErrorBoundary>
                </Route>
                <Route path="/todo.txt">
                    <pre>{taskActions.util.createTasksFile(tasks)}</pre>
                </Route>
                <Route path="/task/:taskId">
                    {
                        React.createElement(function () {
                            const { taskId } = useParams();
                            return (
                                <ErrorBoundary>
                                    <div>
                                        <Link to="/">Todos</Link>
                                        {" > "}
                                        This task
                                </div>
                                    <TaskItem task={
                                        tasks[parseInt(taskId)]
                                    }
                                        expand="true" />
                                </ErrorBoundary>
                            );
                        })
                    } </Route>
                <Route path="/drivePicker">
                    <ErrorBoundary>
                        <DrivePicker />
                    </ErrorBoundary>
                </Route>
                <Route exact path="/settings">
                    <ErrorBoundary>
                        <Settings />
                    </ErrorBoundary>
                </Route>
                <Route exact path="/help">
                    <div>
                        <h5>About this App</h5>
                        <p>This is meant for easily tracking time throughout the work day to see where it is all being spent.  Additionally it enables you to try to stay more focused through the pomodoro timer technique.</p>
                        <p>You can find the source for this <a href="https://gitlab.com/snydergd91/focustask">on gitlab.</a></p>
                        <h5>Troubleshooting</h5>
                        <h6>Problem: Notification doesn't show up when the app isn't open on my phone</h6>
                        <p>Currently we only support using <a href="https://web.dev/notification-triggers/">Notification Triggers</a> for delayed/timed notifications in the background.  However, this is only an experimental feature in Chrome.  We hope it will become standard across several browsers in the future.  To enable it, for now, go to chrome:flags in chrome and enable "Experimental Web Platform Features".  The only alternative we have found for providing this functionality is using push events that go through a cloud service.  This would require additonal cost on our part, mean your app wouldn't work offline, and mean your notification was going through a cloud service.</p>
                        <h6>Problem: Notification doesn't pop up when chrome is in the background on my computer.</h6>
                        <p>In this case, you may have not given the app notification permissions.  You should see a little bell show up in the timer section of the app.  Clicking it will request the notification permission.  You should then be prompted to allow notifications.  In some browsers, you may see a little icon show up in the address bar telling you the notifications have been requested/blocked, and you'll need to enable them by clicking on that.  If you are unable to (e.g., group policy on a managed machine), your best option may be to just have the window open and watch for the standard "alert" box to pop up inside the window.</p>
                        <h5>Supported Browsers</h5>
                        <ul>
                            <li>Chrome (latest)</li>
                            <li>Edge (latest)</li>
                            <li>Firefox (latest)</li>
                            <li>Safari (latest)</li>
                        </ul>
                    </div>
                </Route>
                <div className="my-3">
                    {
                        !online ? (
                            <div className="alert alert-warning">In offline mode</div>
                        ) : authorizationNeeded ? (
                            <button type="button" className="btn btn-primary"
                                onClick={
                                    drive.signIn
                                }>
                                Connect with Google Drive
                        </button>
                        ) : (username && (
                            <>
                                <p>
                                    Your user is{" "}
                                    <b>{username}</b>.
                                <button type="button" className="btn btn-sm btn-link"
                                        onClick={
                                            drive.signOut
                                        }>
                                        Sign Out
                                </button>
                                </p>
                                <p> {
                                    synced ? "All changes synced." : "Sync pending..."
                                }
                                    <button type="button" className="btn btn-link btn-sm"
                                        onClick={
                                            storage.saveChanges
                                        }>
                                        Sync Now
                                </button>
                                    <Link to="/drivePicker" className="btn btn-link btn-sm">
                                        View Stored Files
                                </Link>
                                </p>
                            </>
                        ))
                    }
                </div>
            </div>
        </Router>
    );
}

function mapStateToProps(state, ownProps) {
    return {
        username: state.filepicker.email,
        authorizationNeeded: !state.filepicker.signedIn && state.filepicker.loaded,
        error: state.app.error,
        synced: state.timer.lastChanged <= state.timer.lastSaved && state.tasks.lastChanged <= state.tasks.lastSaved,
        online: state.app.online,
        ...ownProps,
        tasks: state.tasks.tasks,
        dateRange: state.app.dateRange
    };
}
function mapDispatchToProps(dispatch) {
    return {
        setDateRangeStart: (date) => dispatch(appActions.setDateRangeStart(date)),
        setDateRangeEnd: (date) => dispatch(appActions.setDateRangeEnd(date))
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(App);
