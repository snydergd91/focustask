import React from "react";
import { connect } from "react-redux";
import * as timerActions from "../ducks/timer";
import { requestNotifications } from "../util/browser.js";
import { formatSecondsAsTime } from "../util/time.js";

export function Timer({
    className = "",
    currentStage,
    stages,
    target,
    notificationsAvailable,
    running,
    alarming,
    startTime,
    pausedTimeCompleted,
    onStart,
    onPause,
    onStop,
    onStageChange,
    onReset,
    onUnlinkTarget,
} = {}) {
    const currentSeconds = Math.floor(
        (running ? new Date().getTime() - startTime : pausedTimeCompleted) / 1000
    );
    const totalSeconds = stages[currentStage].time;
    const completeRatio = Math.min(currentSeconds / totalSeconds, 1);
    const percent = Math.floor(completeRatio * 100);
    const outerClasses = className.split(" ").filter((x) => x !== "card");
    const [ticks, setTicks] = React.useState(0);

    React.useEffect(() => {
        const handle = window.setInterval(() => {
            setTicks(ticks + 1);
        }, 1000);
        return () => window.clearInterval(handle);
    }, [startTime, ticks, setTicks]);
    outerClasses.push("card");

    return (
        <div className={outerClasses.join(" ")}>
            <div className="card-body">
                <div>
                    <div
                        style={{
                            backgroundColor: `hsl(${100 - percent}, 100%, 30%)`,
                            position: "relative",
                            height: "auto",
                        }}
                        className="progress"
                    >
                        <div
                            style={{
                                position: "absolute",
                                left: 0,
                                top: 0,
                                width: "100%",
                                height: "100%",
                                zIndex: 1,
                            }}
                        >
                            <div
                                className="progress-bar"
                                role="progressbar"
                                aria-valuenow={percent}
                                aria-valuemin="0"
                                aria-valuemax="100"
                                style={{
                                    width: `${completeRatio * 100}%`,
                                    height: "100%",
                                    backgroundColor: `hsl(${100 - percent},100%, 50%)`,
                                }}
                            ></div>
                        </div>
                        <h5
                            style={{
                                color: "black",
                                textAlign: "center",
                                width: "100%",
                                zIndex: 2,
                            }}
                            className="my-1"
                        >
                            {formatSecondsAsTime(currentSeconds)} /{" "}
                            {formatSecondsAsTime(totalSeconds)}
                        </h5>
                    </div>
                </div>
                <div>
                    {stages.map((x, i) => (
                        <button
                            type="button"
                            key={i}
                            className={`btn btn-${
                                (currentStage === i ? "" : "outline-") +
                                (x.isBreak ? "primary" : "info")
                                }`}
                            onClick={() => onStageChange(i)}
                        >
                            {formatSecondsAsTime(x.time)}
                        </button>
                    ))}
                </div>
                <div className="d-flex flex-wrap justify-content-end">
                    <div className="flex-grow-1">
                        {alarming ? (
                            <>
                                Done with task (next up is
                <b>
                                    {" " +
                                        (stages[(currentStage + 1) % stages.length].isBreak
                                            ? "break"
                                            : target || "N/A")}
                                </b>
                                )
              </>
                        ) : (
                                <>
                                    Currently working on:{" "}
                                    <b>{stages[currentStage].isBreak ? "break" : target || "N/A"}</b>
                                </>
                            )}
                    </div>
                    <div className="btn-group">
                        {!notificationsAvailable && (
                            <button
                                type="button"
                                className="btn btn-warning"
                                onClick={requestNotifications}
                                title="Enable Notifications"
                            >
                                <i className="fa fa-bell" />
                                <span className="sr-only">Enable Notifications</span>
                            </button>
                        )}
                        {target && (
                            <button type="button"
                                className="btn btn-secondary"
                                onClick={onUnlinkTarget}
                                title="Unlink from task">
                                <span className="sr-only">Unlink from task</span>
                                <i className="fa fa-unlink" />
                            </button>
                        )}
                        <button
                            type="button"
                            className="btn btn-danger"
                            onClick={onReset}
                            title="Reset"
                        >
                            <i className="fa fa-undo" />
                            <span className="sr-only">Reset</span>
                        </button>
                        {running && (
                            <button
                                type="button"
                                className="btn btn-secondary"
                                onClick={onStop}
                            >
                                <i className="fa fa-stop" />
                                <span className="sr-only">Stop</span>
                            </button>
                        )}
                        {running ? (
                            <button
                                type="button"
                                className="btn btn-primary"
                                onClick={onPause}
                                title="Pause"
                            >
                                <i className="fa fa-pause" />
                                <span className="sr-only">Pause</span>
                            </button>
                        ) : (
                                <button
                                    type="button"
                                    className="btn btn-primary"
                                    onClick={onStart}
                                    title="Start"
                                >
                                    <i className="fa fa-play" />
                                    <span className="sr-only">Start</span>
                                </button>
                            )}
                    </div>
                </div>
            </div>
        </div>
    );
}

function mapStateToProps(state, ownProps) {
    return {
        startTime: state.timer.startTime,
        pausedTimeCompleted: state.timer.pausedTimeCompleted,
        target: state.timer.target.title,
        currentStage: state.timer.currentStage,
        stages: state.timer.stages,
        notificationsAvailable: state.app.notificationsAvailable,
        running: state.timer.running,
        alarming: state.timer.alarming,
    };
}
function mapDispatchToProps(dispatch) {
    return {
        onStart: () => dispatch(timerActions.startTimer()),
        onPause: () => dispatch(timerActions.pauseTimer()),
        onStageChange: (stage) =>
            dispatch(timerActions.setTimerCurrentStage(stage)),
        onReset: () => dispatch(timerActions.resetTimer()),
        onStop: () => dispatch(timerActions.setTimerRunning(false)),
        onUnlinkTarget: () => dispatch(timerActions.setTimerTarget(false, false)),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(Timer);
