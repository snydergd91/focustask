import React from "react";
import { connect } from "react-redux";
import * as browserPersist from "../util/browserPersist";
import * as themes from "../util/themes";
import { util as taskUtil } from "../ducks/tasks";
import * as taskActions from "../ducks/tasks";

export function Settings({ taskSort, changeTaskSort }) {
    return <div>
        <div className="mb-3">
            <h5>Todo Sort</h5>
            <select value={taskSort} onChange={e => changeTaskSort(e.target.value)}>
                {Object.entries(taskUtil.sortOptions).map(([key, value]) => <option value={key} key={key}>{value.title}</option>)}
            </select>
        </div>
        <div className="mb-3">
            <h5>Theme</h5>
            <button className="btn btn-primary"
                onClick={
                    themes.chooseTheme
                }>
                Change Theme
            </button>
        </div>
        <div className="mb-3">
            <h5>Local Data</h5>
            <p>We save several pieces of application state to your browser's storage for this page.  During some updates it might be possible that this could be a problem.  In those cases, clearing the data here may fix the problem.</p>
            <p><strong>This will reset the application as if you have never used it before.</strong></p>
            <button type="button" className="btn btn-primary"
                onClick={
                    browserPersist.reset
                }>
                Clear Local Data
            </button>
        </div>
        <hr />
    </div>;
}

export default connect(state => ({
    taskSort: state.tasks.sort,
}), dispatch => ({
    changeTaskSort: (sort) => dispatch(taskActions.setSort(sort)),
}))(Settings);
