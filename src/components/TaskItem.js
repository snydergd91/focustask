import React from "react";
import { connect } from "react-redux";
import * as taskActions from "../ducks/tasks";
import * as timerActions from "../ducks/timer";
import * as timeUtil from "../util/time";
import SpanView from "./SpanView";
import TextEdit from "./TextEdit";

function TaskItem({
  task,
  active,
  updateTask,
  completeTask,
  orderUp,
  orderDown,
  startTimer,
  spans,
  addEmptySpan,
  expand = false,
}) {
  const [detailsExpanded, setDetailsExpanded] = React.useState(expand);

  return (
    <div
      className={`card my-3${
        active ? " border-primary" : task.deleted ? " border-danger" : ""
      }`}
    >
      <div className="card-header py-1 px-2 d-flex flex-wrap justify-content-end">
        <h4 className="card-title m-0 p-0 flex-grow-1">
          <TextEdit
            value={task.title}
            onChange={(title) => updateTask({ ...task, title })}
          />
        </h4>
        <div className="btn-group px-0">
          <button className="btn btn-primary btn-small" onClick={startTimer}>
            <span className="sr-only">Start Timer</span>
            <i className="fa fa-play" title="Start Timer" />
          </button>
          <button
            className="btn btn-secondary btn-small"
            onClick={() => setDetailsExpanded(!detailsExpanded)}
          >
            <span className="sr-only">Show Details</span>
            <i className="fa fa-gear" title="Show Details" />
          </button>
        </div>
      </div>
      <ul className="list-group list-group-flush">
        {detailsExpanded && (
          <>
            <li className="list-group-item">
              <div>
                Duration:{" "}
                {timeUtil.formatSecondsAsTime(
                  spans
                    ? spans.reduce(
                        (sum, x) => sum + parseInt(x.duration / 1000),
                        0
                      )
                    : 0
                )}
                Order: {task.order}
              </div>
              <div>
                <button
                  className="btn btn-secondary btn-small"
                  onClick={orderUp}
                  title="Reorder up"
                >
                  <span className="sr-only">Reorder up</span>
                  <i className="fa fa-arrow-up" />
                </button>
                <button
                  className="btn btn-secondary btn-small"
                  onClick={orderDown}
                  title="Reorder down"
                >
                  <span className="sr-only">Reorder down</span>
                  <i className="fa fa-arrow-down" />
                </button>
                <button
                  className="btn btn-secondary btn-small"
                  onClick={() => addEmptySpan(task)}
                  title="Add Span"
                >
                  <span className="sr-only">Add Span</span>
                  <i className="fa fa-plus" />
                </button>
                {task.complete || (
                  <button
                    className="btn btn-success btn-small"
                    onClick={completeTask}
                  >
                    <span className="sr-only">Delete</span>
                    <i className="fa fa-check" title="Delete" />
                  </button>
                )}
              </div>
            </li>
            {spans &&
              spans.map((span) => (
                <li className="list-group-item" key={span.id}>
                  <SpanView span={span} key={span.id} />
                </li>
              ))}
          </>
        )}
      </ul>
    </div>
  );
}

export default connect(
  (state, { task }) => ({
    spans: Object.values(state.timer.spans)
      .map((span) => ({
        ...span,
        start: new Date(span.end - span.duration),
        end: new Date(span.end),
        original: span,
      }))
      .filter(
        (x) =>
          !x.deleted &&
          x.start.getTime() >= state.app.dateRange.start &&
          x.start.getTime() <= state.app.dateRange.end &&
          x.taskId === task.id
      )
      .sort((a, b) => b.end - b.duration - (a.end - a.duration)),
  }),
  (dispatch, { task, active }) => ({
    completeTask: () => dispatch(taskActions.completeTask(task)),
    orderDown: () => dispatch(taskActions.reorderTask(task, -1)),
    orderUp: () => dispatch(taskActions.reorderTask(task, +1)),
    startTimer: () => dispatch(timerActions.setTimerTarget(task)),
    addEmptySpan: () => dispatch(timerActions.addEmptySpan(task)),
    updateTask: (updated) => {
      if (active) {
        dispatch(timerActions.setTimerTarget(updated, false));
      }
      dispatch(taskActions.updateTask(updated));
    },
  })
)(TaskItem);
