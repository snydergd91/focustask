import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import * as taskActions from "../ducks/tasks";
import TaskItem from "./TaskItem.js";

export function TaskList({ loading, tasks, activeTask, onTaskCreated, spans, taskOrder, dateRange }) {
    const [pendingName, setPendingName] = React.useState("");

    return (
        <div>
            <div>{loading ? " (loading...)" : "\xA0"}</div>
            <p className="input-group">
                <input
                    type="text"
                    className="form-control"
                    onChange={(e) => setPendingName(e.target.value)}
                    value={pendingName}
                    onKeyPress={(e) => {
                        if (e.key == "Enter") {
                            onTaskCreated(pendingName);
                            setPendingName("");
                        }
                    }}
                />
                <span className="input-group-append">
                    <button
                        type="button"
                        className="btn btn-primary"
                        onClick={(e) => {
                            onTaskCreated(pendingName);
                            setPendingName("");
                        }}
                    >
                        Add
                    </button>
                    <button
                        type="button"
                        className="btn btn-secondary"
                        onClick={(e) => {
                            onTaskCreated(pendingName, true);
                            setPendingName("");
                        }}
                        title="Start"
                    >
                        <span className="sr-only">Start</span>
                        <i className="fa fa-play" />
                    </button>
                </span>
            </p>
            {tasks &&
                (function () {
                    const keys = taskActions.util.sortOptions[taskOrder].taskKeyLister(tasks, spans, dateRange);
                    if (keys.length) {
                        if (keys[0].length == 2) {
                            return keys.map(([title, taskKeys]) => {
                                return <div key={title}>
                                    <h5>{title}</h5>
                                    {taskKeys.map(x => {
                                        let task = tasks[x];
                                        return <TaskItem {...{ task, active: activeTask == task.id, key: x }} />;
                                    })}
                                </div>
                            });
                        } else {
                            return keys.map((x) => {
                                let task = tasks[x];
                                return (
                                    <TaskItem
                                        {...{
                                            task,
                                            active: activeTask == task.id,
                                            key: x,
                                        }}
                                    />
                                );
                            });
                        }
                    }
                })()}
            <Link to="/todo.txt">View as todo.txt</Link>
        </div>
    );
}

function mapStateToProps(state, ownProps) {
    return {
        activeTask: state.timer && state.timer.target && state.timer.target.id,
        tasks: Object.fromEntries(
            Object.entries(state.tasks.tasks).filter(([, v]) => !v.deleted)
        ),
        loading: state.tasks.loading,
        spans: Object.values(state.timer.spans),
        taskOrder: state.tasks.sort,
        dateRange: state.app.dateRange,
    };
}
function mapDispatchToProps(dispatch) {
    return {
        onTaskCreated: (taskName, startTimer) => {
            dispatch(taskActions.createTask(taskName, startTimer));
        },
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(TaskList);
