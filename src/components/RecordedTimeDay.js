import React from "react";
import dateFormat from "dateformat";
import SpanView from "./SpanView";

function tryFormat() {
  try {
    return dateFormat.apply(this, arguments);
  } catch (e) {
    return (
      e.toString() + " -- " + [...arguments].map((x) => x.toString()).join(",")
    );
  }
}
function RecordedTimeDay({ day, onDeleteSpan }) {
  return (
    <section className="mb-4" key={day.dateObj.getTime()}>
      <h5>{tryFormat(day.dateObj, "dddd - mmmm d, yyyy")}</h5>
      {day.spans.map((span) => (
        <div className="card mb-2" key={span.id}>
          <div className="card-body py-1">
            <SpanView span={span} onDelete={onDeleteSpan} />
          </div>
        </div>
      ))}
    </section>
  );
}

export default RecordedTimeDay;
