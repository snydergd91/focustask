import dateFormat from "dateformat";
import React from "react";
import Calendar from "./Calendar";

export default function DateEdit({ value, onUpdate }) {
  const [edit, setEdit] = React.useState(false);
  const [date, setDate] = React.useState(value);

  return (
    <span style={{ display: "inline-table" }}>
      {edit ? (
        <>
          <Calendar
            selectedDate={date}
            dateSelected={setDate}
            actions={
              <>
                {" "}
                <button
                  className="btn btn-success"
                  onClick={() => {
                    onUpdate(date);
                    setEdit(false);
                  }}
                  title="Done"
                >
                  <span className="sr-only">Done</span>
                  <i className="fa fa-check" />
                </button>{" "}
                <button
                  className="btn btn-secondary"
                  onClick={() => setEdit(false)}
                  title="Cancel"
                >
                  <span className="sr-only">Cancel</span>
                  <i className="fa fa-close" />
                </button>
              </>
            }
          />
        </>
      ) : (
        <>
          <button
            className="btn btn-default"
            onClick={() => {
              setDate(value);
              setEdit(true);
            }}
            title="Edit"
          >
            <span className="sr-only">Edit</span>
            <i className="fa fa-pencil" />
          </button>
          {dateFormat(value, "yyyy-mm-dd")}
        </>
      )}
    </span>
  );
}
