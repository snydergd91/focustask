import showDialog from "../util/showDialogJquery";
import delayedResource from "./delayedResource";
import * as appActions from "../ducks/app";
import React from "react";

function createThemeEl(url) {
  const el = document.createElement("link");
  el.href = url;
  el.rel = "stylesheet";
  return el;
}
const themes = {
  dark2: {
    el: createThemeEl("https://bootswatch.com/4/cyborg/bootstrap.min.css"),
    title: "Dark - Cyborg",
    description: (<>
    By <a href="https://bootswatch.com/cyborg/">Bootswatch</a>
    </>)
  },
  dark3: {
    el: createThemeEl("https://bootswatch.com/4/darkly/bootstrap.min.css"),
    title: "Dark - Darkly",
    description: (<>
    By <a href="https://bootswatch.com/darkly/">Bootswatch</a>
    </>)
  },
  dark: {
    el: createThemeEl(
      "https://unpkg.com/@forevolve/bootstrap-dark@1.0.0-alpha.1091/dist/css/bootstrap-dark.min.css"
    ),
    title: "Dark - Bootstrap Dark",
    description: (
      <>
        Visit on{" "}
        <a href="https://github.com/ForEvolve/bootstrap-dark">Github</a> -- has some issues (with <code>{"<pre>"}</code>, for example).
      </>
    ),
  },
  light: {
    title: "Light - Unthemed",
    description: "Unthemed, \"light\", Bootstrap",
  },
  sketchy: {
    el: createThemeEl("https://bootswatch.com/4/sketchy/bootstrap.min.css"),
    title: "Light - Sketchy",
    description: (<>
    By <a href="https://bootswatch.com/sketchy/">Bootswatch</a>
    </>)
  },
  journal: {
    el: createThemeEl("https://bootswatch.com/4/journal/bootstrap.min.css"),
    title: "Light - Journal",
    description: (<>
      By <a href="https://bootswatch.com/journal/">Bootswatch</a>
      </>)
    },
  cerulean: {
    el: createThemeEl("https://bootswatch.com/4/cerulean/bootstrap.min.css"),
    title: "Light - Cerulean",
  },
};
let currentTheme = false;
function setTheme(theme) {
  if (theme !== currentTheme) {
    if (currentTheme in themes && "el" in themes[currentTheme]) {
      document.head.removeChild(themes[currentTheme].el);
    }
    if (theme in themes && "el" in themes[theme]) {
      document.head.appendChild(themes[theme].el);
    }
    currentTheme = theme;
  }
}

const storePromise = delayedResource();
export function setupThemeChanging(store) {
  storePromise.resolve(store);
  setTheme(store.getState().app.theme);
  store.subscribe(() => {
    setTheme(store.getState().app.theme);
  });
}

export async function chooseTheme() {
  const store = await storePromise;
  const state = store.getState();

  function ThemeChooser() {
    const [theme, setTheme] = React.useState(state.app.theme);
    return Object.entries(themes).map(([name, { title, description = "" }]) => (
      <div key={name}>
        <button
          className={`btn btn-${name === theme ? "primary" : "secondary"}`}
          onClick={() => {
            setTheme(name);
            store.dispatch(appActions.setTheme(name));
          }}
        >
          {title}
        </button>
        {description && <> -- {description}</>}
      </div>
    ));
  }
  showDialog({
    title: "Choose theme",
    body: <ThemeChooser />,
  });
}
