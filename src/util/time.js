
export const formatSecondsAsTime = (time) => {
    time = Math.floor(time);
    let result = '';
    let unit;
    for (let count = 0; time > 0 || count < 2; count++) {
        [unit, time] = [time % 60, Math.floor(time / 60)];
        result = ':' + (unit > 9 ? '' : '0') + unit + result;
    }
    return result.substr(1);
}

const pad = (n, char, input) => new Array(Math.max(0, n - (input.toString().length || 0))).fill(char).join('') + input;
export const formatTimestamp = (ts) => {
    const date = new Date(ts);
    const hours = date.getHours();
    const minutes = date.getMinutes();
    return `${date.getFullYear()}-${pad(2, '0', date.getMonth())}-${pad(2, '0', date.getDate())} ${pad(2, '0', hours)}:${pad(2, '0', minutes)}`;
};

export const parts = (date) => ({
    year: date.getFullYear(),
    month: date.getMonth() + 1,
    day: date.getDate(),
    hour: date.getHours(),
    minute: date.getMinutes(),
    second: date.getSeconds(),
    millisecond: date.getMilliseconds(),
})

export const fromParts = (parts) => {
    return new Date(parts.year, parts.month - 1, parts.day, parts.hour, parts.minute, parts.second, parts.millisecond)
};

export const extractDate = (datetime) => {
    const myParts = parts(datetime);
    return fromParts({...parts(new Date(0)), ...Object.fromEntries(["year", "month", "day"].map(x => [x, myParts[x]]))});
}