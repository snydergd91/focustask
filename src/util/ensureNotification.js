const previousNotifications = {};

function deepEqual(object1, object2) {
  if (typeof object1 !== typeof object2) return false;
  if (typeof object1 !== "object") return object1 === object2;
  const keys1 = Object.keys(object1);
  const keys2 = Object.keys(object2);

  if (keys1.length !== keys2.length) {
    return false;
  }

  for (let key of keys1) {
    if (!deepEqual(object1[key], object2[key])) {
      return false;
    }
  }

  return true;
}
function isChanged(tag, dependencies) {
  previousNotifications[tag] = previousNotifications[tag] || {};
  const previous = previousNotifications[tag].oldState;
  previousNotifications[tag].oldState = dependencies;

  if (!previous) {
    return true;
  } else {
    for (let i in dependencies) {
      if (!deepEqual(previous[i], dependencies[i])) {
        return true;
      }
    }
    return false;
  }
}

export default async function ensureNotification(tag, time, options) {
  const timeout = time - new Date().getTime();
  const notificationOpts = {
    body: "Your timer just went off!",
    renotify: true,
    tag: tag,
    badge: "img/tomato.png",
    icon: "img/tomato.png",
    vibrate: [200, 100, 200, 100, 200, 100, 200],
    title: "Timer went off!",
    ...options,
  };

  if (
    isChanged(tag, [
      timeout,
      notificationOpts,
      "Notification" in window && Notification.permission === "granted",
    ])
  ) {
    if (
      tag in previousNotifications &&
      previousNotifications[tag]["notifierCancel"]
    ) {
      await previousNotifications[tag].notifierCancel();
    }
    if (time && timeout > 0) setupNotifications();
  }
  function setupNotifications() {
    if ("Notification" in window && Notification.permission === "granted") {
      const notify = async () => {
        if ("showNotification" in ServiceWorkerRegistration.prototype) {
            (await navigator.serviceWorker.ready).showNotification(notificationOpts.title, notificationOpts);
        } else {
            new Notification(notificationOpts.title, notificationOpts);
        }
      };
      if ("showTrigger" in Notification.prototype) {
        notificationOpts["showTrigger"] = new TimestampTrigger(
          new Date().getTime() + timeout
        );
        notify();
        previousNotifications[tag].notifierCancel = async () => {
          await navigator.serviceWorker.ready;
          const reg = await navigator.serviceWorker.getRegistration();
          const activeNotifications = await reg.getNotifications({
            tag: tag,
            includeTriggered: true,
          });
          activeNotifications.forEach((notification) => {
            notification.close();
          });
        };
      } else {
        const handle = window.setTimeout(notify, Math.max(timeout, 0));
        previousNotifications[tag].notifierCancel = async () => {
          window.clearTimeout(handle);
        };
      }
    } else {
      const handle = window.setTimeout(
        () =>
          alert(
            `${notificationOpts.title || ""} ${notificationOpts.body || ""}`
          ),
        Math.max(timeout, 0)
      );
      previousNotifications[tag].notifierCancel = async () => {
        window.clearTimeout(handle);
      };
    }
  }
}
