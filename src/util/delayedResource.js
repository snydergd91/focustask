export default function () {
  let resolve, reject;
  const myReturn = new Promise(function () {
    [resolve, reject] = [...arguments];
  });

  myReturn.resolve = resolve;
  myReturn.reject = reject;
  myReturn.fullfilled = false;
  myReturn.success = null;
  myReturn.result = null;

  myReturn.then(function () {
    myReturn.result = [...arguments];
    myReturn.success = true;
    myReturn.fullfilled = true;
  });
  myReturn.catch(function () {
    myReturn.result = [...arguments];
    myReturn.success = false;
    myReturn.fullfilled = true;
  });
  return myReturn;
}
