import localForage from "localforage";

export async function getInitialState() {
  return localForage
    .getItem("focustaskState")
    .then((x) => (x && JSON.parse(x)) || {});
}
export function setupLocalPersistence(store) {
  store.subscribe(() => {
    const state = store.getState();
    const saveState = {
      timer: {
        target: state.timer.target,
        currentStage: state.timer.currentStage,
        targetStartTime: state.timer.targetStartTime,
        startTime: state.timer.startTime,
        running: state.timer.running,
        spans: state.timer.spans,
        lastSaved: state.timer.lastSaved,
        lastChanged: state.timer.lastChanged,
        pausedTimeCompleted: state.timer.pausedTimeCompleted,
      },
      tasks: state.tasks,
      filePicker: state.filePicker,
      app: { theme: state.app.theme },
    };
    localForage.setItem("focustaskState", JSON.stringify(saveState));
  });
}
export function reset() {
    localForage.clear();
    window.location.reload();
}