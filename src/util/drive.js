/// <reference path="../../node_modules/@types/gapi/index.d.ts" />
/// <reference path="../../node_modules/@types/gapi.auth2/index.d.ts" />
/// <reference path="../../node_modules/@types/gapi.client.drive/index.d.ts" />
/// <reference path="../../node_modules/@types/gapi.drive/index.d.ts" />

// this is so that the types are still active from above.  Otherwise, would do import normally.
import gapiPromise from "./gapi";
/* eslint-disable no-native-reassign */
/* globals google, gapi */
window.gapi = {};

gapiPromise.then(
  (g) => (window.gapi = g),
  (e) => console.log(e)
);

const appId =
  "301712555268-h9mkd0j9c9e36oe6sdu299aclqfth1s6.apps.googleusercontent.com";
const apiKey = "AIzaSyCcTw7aP9hPoqgQGT5IpCLiKZyykpzylK0";

const events = (function () {
  const eventCallbacks = {};
  return {
    dispatch(eventName, data) {
      if (eventName in eventCallbacks) {
        for (let cb of eventCallbacks[eventName]) {
          cb(data);
        }
      }
    },
    addEventListener(eventName, callback) {
      if (eventName in eventCallbacks) {
        eventCallbacks[eventName].add(callback);
      } else {
        eventCallbacks[eventName] = new Set([callback]);
      }
    },
    removeEventListener(eventName, callback) {
      if (eventName in eventCallbacks) {
        eventCallbacks[eventName].delete(callback);
      }
    },
  };
})();

const loaded = gapiPromise
  .then(
    () =>
      new Promise((resolve, reject) =>
        gapi.load("client:auth2", resolve, reject)
      )
  )
  .then(() =>
    gapi.client.init({
      apiKey: apiKey,
      clientId: appId,
      discoveryDocs: [
        "https://www.googleapis.com/discovery/v1/apis/drive/v3/rest",
      ],
      scope: "https://www.googleapis.com/auth/drive.appdata",
    })
  );

const drive = {
  addEventListener: events.addEventListener,
  removeEventListener: events.removeEventListener,
  isSignedIn: () =>
    drive.loadStatus !== "loading" &&
    gapi.auth2.getAuthInstance().isSignedIn.get(),
  signOut: () => {
    gapi.auth2.getAuthInstance().signOut();
    events.dispatch("signInChange", { signedIn: false });
  },
  loadStatus: "loading",
  loaded,
  ready: loaded.then(
    () =>
      new Promise((resolve, reject) => {
        gapi.auth2.getAuthInstance().isSignedIn.listen((loggedIn) => {
          events.dispatch("signInChange", { signedIn: loggedIn });
          if (loggedIn) resolve(drive);
          else reject();
        });
        if (gapi.auth2.getAuthInstance().isSignedIn.get()) {
          events.dispatch("signInChange", { signedIn: true });
          resolve(drive);
        } else {
          events.dispatch("authneeded");
        }
      })
  ),
  signIn: () => {
    const options = new gapi.auth2.SigninOptionsBuilder();
    options.setPrompt("select_account");
    gapi.auth2.getAuthInstance().signIn(options);
  },
  listFiles() {
    return gapi.client.drive.files
      .list({
        spaces: "appDataFolder",
        pageSize: 10,
        fields: "nextPageToken, files(id, name)",
      })
      .then((x) => x.result.files);
  },
  /**
   * Save file by fileId in Google Drive
   *
   * @param {string} fileId
   * @param {any} data
   */
  saveDataToId(fileId, data) {
    return gapi.client.request({
      path: `/upload/drive/v3/files/${fileId}`,
      method: "PATCH",
      params: { uploadType: "media" },
      body: data,
    });
  },
  saveContent(name, data) {
    return drive.getFile(name).then((file) => {
      if (file) {
        return drive.saveDataToId(file.id, data);
      } else {
        return gapi.client.drive.files
          .create({ name: name, parents: ["appDataFolder"] })
          .then((response) => drive.saveDataToId(response.result.id, data));
      }
    });
  },
  getFileById(fileId) {
    return gapi.client.drive.files.get({ fileId });
  },
  getFile(name) {
    return gapi.client.drive.files
      .list({
        spaces: "appDataFolder",
        pageSize: 10,
        q: `name='${name}'`,
      })
      .then((response) => response.result.files[0]);
  },
  getContentById(fileId) {
    return gapi.client.drive.files
      .get({
        fileId,
        alt: "media",
      })
      .then((response) => response.body);
  },
  getContent(name) {
    return drive
      .getFile(name)
      .then((file) => drive.getFileById(file.id))
      .then((response) => response.body);
  },
  deleteFileWithId(fileId) {
    return gapi.client.drive.files.delete({ fileId });
  },
  deleteFile(name) {
    return drive.getFile(name).then((f) => drive.deleteFileWithId(f.id));
  },
  renameFileWithId(fileId, name) {
    return gapi.client.drive.files.update({
      fileId,
      name,
    });
  },
  renameFile(name1, name2) {
    gapi.client.drive.files.watch();
    return drive.getFile(name1).then((file) =>
      gapi.client.drive.files.update({
        fileId: file.id,
        name: name2,
      })
    );
  },
  getEmail() {
    return gapi.auth2
      .getAuthInstance()
      .currentUser.get()
      .getBasicProfile()
      .getEmail();
  },
};
drive.loaded
  .then(() => (drive.loadStatus = "loaded"))
  .catch(() => (drive.loadStatus = "failed"));
export default drive;
