import * as taskActions from "../ducks/tasks";
import * as timerActions from "../ducks/timer";
import * as appActions from "../ducks/app";
import * as timeUtil from "../util/time";
import delayedResource from "../util/delayedResource";
import drive from "./drive";

const saveFileName =
  window.location.hostname.startsWith("localhost") ||
  window.location.hostname.startsWith("cloud9dev")
    ? "devData.json"
    : "prodData.json";

let syncCallback = delayedResource();
export const saveChanges = async () => (await syncCallback)(true);

// purge after 7 days of being deleted (assume at that point that deletion has been synced everywhere)
const unpurgedObject = (unfilteredObject) =>
  Object.fromEntries(
    Object.entries(unfilteredObject).filter(
      ([, v]) =>
        !v.deleted ||
        v.updated >=
          timeUtil
            .fromParts({
              ...timeUtil.parts(new Date()),
              day: new Date().getDate() - 7,
            })
            .getTime()
    )
  );
function mapStateToDocument(state) {
  return {
    timer: {
      running: state.timer.running,
      startTime: state.timer.startTime,
      currentStage: state.timer.currentStage,
      targetStartTime: state.timer.targetStartTime,
      pausedTimeCompleted: state.timer.pausedTimeCompleted,
      target: state.timer.target,
      lastTimerChange: state.timer.lastTimerChange,
    },
    tasks: unpurgedObject(state.tasks.tasks),
    spans: unpurgedObject(state.timer.spans),
  };
}
function saveDocumentToState(document, store) {
  const state = store.getState();
  if (
    document.timer &&
    document.timer.lastTimerChange &&
    state.timer.lastTimerChange < document.timer.lastTimerChange
  ) {
    store.dispatch(timerActions.restoreTimerState(document.timer));
  }
  store.dispatch(taskActions.putTasks(document.tasks || false));
  store.dispatch(timerActions.putSpans(document.spans || false));
}

const syncAfter = 15 * 1000; // 15 seconds -- autosync
export async function setupStorage(store) {
  let savedTimeout,
    lastTriggeredChangeTime = 0,
    lastLoad = Math.min(
      store.getState().timer.lastLoad,
      store.getState().tasks.lastLoad
    );
  const syncCallbacks = new Set();
  const sync = function (force) {
    const state = store.getState();
    if (
      force ||
      state.timer.lastChanged > state.timer.lastSaved ||
      state.tasks.lastChanged > state.tasks.lastSaved
    ) {
      // perform sync
      drive.ready.then(() => {
        drive
          .getFile(saveFileName)
          .then(file => drive.getContentById(file.id))
          .catch(() => "{}")
          .then((content) => {
            saveDocumentToState(JSON.parse(content), store);
          })
          .catch((e) => {
            store.dispatch(appActions.triggerError(e));
            saveDocumentToState({}, store);
          });
      });
      return new Promise((resolve) => {
        const cb = () => {
          resolve();
          syncCallbacks.delete(cb);
        };
        syncCallbacks.add(cb);
      });
    } else {
      return Promise.resolve();
    }
  };
  syncCallback.resolve(sync);
  store.subscribe(async () => {
    const state = store.getState();
    const lastChanged = Math.max(
      state.timer.lastChanged,
      state.tasks.lastChanged
    );
    if (lastLoad < Math.min(state.timer.lastLoad, state.tasks.lastLoad)) {
      if (
        state.timer.lastChanged > state.timer.lastSaved ||
        state.tasks.lastChanged > state.tasks.lastSaved
      ) {
        // only actually save if there are changes to save (i.e., not if save state is already done)
        await drive.ready;
        drive
          .saveContent(
            saveFileName,
            JSON.stringify(mapStateToDocument(state), null, "  ")
          )
          .then(() => {
            store.dispatch(timerActions.completeSave());
            store.dispatch(taskActions.completeSave());
            for (const cb of syncCallbacks) {
              cb();
            }
          });
      }
      lastLoad = new Date().getTime();
    }
    if (lastChanged > lastTriggeredChangeTime) {
      lastTriggeredChangeTime = lastChanged;
      if (savedTimeout) window.clearTimeout(savedTimeout);
      savedTimeout = window.setTimeout(() => {
        sync();
        savedTimeout = false;
      }, syncAfter);
    }
  });
  sync(true);
}
