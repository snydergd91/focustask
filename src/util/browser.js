import { setNotificationsAvailable } from "../ducks/app";
import * as appActions from "../ducks/app";

export var requestNotifications;
export const serviceWorker =
  "serviceWorker" in navigator
    ? navigator.serviceWorker.register("sw.js")
    : Promise.rejected();
export const notifications = new Promise((resolve, reject) => {
  const handlePermission = (permission) => {
    if (!("permission" in Notification)) Notification.permission = permission;
    if (
      Notification.permission == "denied" ||
      Notification.permission == "default"
    ) {
      reject(Notification.permission);
    } else {
      serviceWorker
        .then(navigator.serviceWorker.ready)
        .then((registration) => resolve(registration));
    }
  };
  requestNotifications = () => {
    try {
      const request = Notification.requestPermission(handlePermission);
      if ("then" in request) request.then(handlePermission);
    } catch (e) {
      reject(e);
    }
  };
  if (Notification.permission == "granted") {
    handlePermission();
  }
});
export function ensureNotificationStatusUpdated(store) {
  notifications.then(() => store.dispatch(setNotificationsAvailable(true)));
}
export function enableWakeLock() {
  function addWakeLockIfVisible() {
    if (document.visibilityState == 'visible') {
      navigator.wakeLock.request("screen");
    }
  }
  if ("wakeLock" in navigator) {
    addWakeLockIfVisible();
    document.addEventListener("visibilitychange", addWakeLockIfVisible);
  }
}
export function ensureOnlineStatusSet(store) {
  window.addEventListener('online',  () => store.dispatch(appActions.setOnlineStatus(true)));
  window.addEventListener('offline',  () => store.dispatch(appActions.setOnlineStatus(false)));
}