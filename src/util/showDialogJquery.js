import React from "react";
import ReactDOM from "react-dom";

export default function showDialog({
  title = "Notification",
  body = false,
  options = [{ title: "OK", data: "ok", type: "primary" }],
}) {
  return new Promise((resolve, reject) => {
    const el = $(`<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
      </div>
    </div>`);
    const reactRoot = el.find(".modal-dialog")[0];
    const dismiss = (data) => {
      el.on("hidden.bs.modal", (e) => {
        el.remove();
        resolve(data);
      });
      ReactDOM.unmountComponentAtNode(reactRoot);
      el.modal("hide");
    };
    const ModalContent = function () {
      return (
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title">{title}</h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          {body && <div className="modal-body">{body}</div>}
          {options && (
            <div className="modal-footer">
              {options.map((option) => (
                <button
                  type="button"
                  key={option.title}
                  className={`btn btn-${option.type || "default"}`}
                  onClick={() => dismiss(option.data)}
                >
                  {option.title}
                </button>
              ))}
            </div>
          )}
        </div>
      );
    };
    el.appendTo($(document.body));
    el.modal("show");
    ReactDOM.render(<ModalContent />, reactRoot);
  });
}
