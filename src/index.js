import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import App from "./components/App";
import configureStore from "./configureStore";
import * as filepickerActions from "./ducks/filepicker";
import * as taskActions from "./ducks/tasks";
import {
  enableWakeLock,
  ensureNotificationStatusUpdated,
  ensureOnlineStatusSet,
} from "./util/browser";
import * as persist from "./util/browserPersist";
import drive from "./util/drive";
import ensureNotification from "./util/ensureNotification";
import * as storage from "./util/storage";
import * as themes from "./util/themes";

async function setup() {
  const preloadedState = await persist.getInitialState();
  const store = configureStore(preloadedState);
  persist.setupLocalPersistence(store);
  drive.loaded.then(() => store.dispatch(filepickerActions.setLoaded()));
  drive.addEventListener("signInChange", ({ signedIn }) => {
    store.dispatch(
      signedIn
        ? filepickerActions.setAuthorized(drive.getEmail())
        : filepickerActions.setLoggedOut()
    );
  });
  drive.addEventListener("authneeded", () => {
    store.dispatch(taskActions.setLoading(false));
  });
  window.drive = drive;

  store.subscribe(() => {
    const state = store.getState();
    const totalTime = state.timer.stages[state.timer.currentStage].time * 1000;
    ensureNotification(
      "pomodoro-timer",
      state.timer.running &&
        state.timer.startTime &&
        state.timer.startTime + totalTime,
      {
        title: "Pomodoro Alarming!",
        body: `Your ${
          Math.floor(totalTime / 1000) / 60
        } minute timer is alarming!`,
      }
    );
  });

  ensureNotificationStatusUpdated(store);
  ensureOnlineStatusSet(store);
  storage.setupStorage(store);
  enableWakeLock();
  themes.setupThemeChanging(store);

  ReactDOM.render(
    <Provider store={store}>
      <App drive={drive} />
    </Provider>,
    document.getElementById("root")
  );
}

setup();
