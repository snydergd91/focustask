/* Action Types */
const SET_ITEMS = "focustask/filepicker/SET_ITEMS";
const SET_LOADED = "focustask/filepicker/SET_LOADED";
const SET_AUTHORIZED = "focustask/filepicker/SET_AUTHORIZED";
const SET_CONTENT = "focustask/filepicker/SET_CONTENT";
const SET_CURRENT_FILE = "focustask/filepicker/SET_CURRENT_FILE";
const SET_LOGGED_OUT = "focustask/filepicker/SET_LOGGED_OUT";

/* Reducer */
export default function (
  state = {
    items: [],
    email: false,
    loaded: false,
    signedIn: false,
    currentContent: false,
    currentFile: "timerdata.json",
  },
  action
) {
  switch (action.type) {
    case SET_ITEMS:
      return { ...state, items: action.items };
    case SET_LOADED:
      return { ...state, loaded: true };
    case SET_AUTHORIZED:
      return { ...state, signedIn: true, email: action.email };
    case SET_CONTENT:
      return { ...state, currentContent: action.content };
    case SET_CURRENT_FILE:
      return { ...state, currentFile: action.filename };
    case SET_LOGGED_OUT:
      return { ...state, signedIn: false, email: false };
  }
  return state;
}

/* Action creators */
export function setItems(items) {
  return { type: SET_ITEMS, items };
}
export function setLoaded() {
  return { type: SET_LOADED };
}
export function setAuthorized(email) {
  return { type: SET_AUTHORIZED, email };
}
export function setContent(content) {
  return { type: SET_CONTENT, content };
}
export function setCurrentFile(filename) {
  return { type: SET_CURRENT_FILE, filename };
}
export function setLoggedOut() {
  return { type: SET_LOGGED_OUT };
}
