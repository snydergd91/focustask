import { util as taskUtil } from "./tasks";
import * as taskActions from "./tasks";

function spansWithAddedBlank(state, end, duration, task) {
    const id = new Date().getTime().toString();
    return {
        ...state.spans,
        [id]: {
            task: taskUtil.taskToLine(task),
            duration: duration,
            end,
            id,
            updated: new Date().getTime(),
            taskId: task.id,
        },
    };
}
function spansWithAdded(state, timestamp) {
    if (
        state.target.id &&
        ((state.running && timestamp - state.targetStartTime) ||
            state.pausedTimeCompleted) &&
        !state.stages[state.currentStage].isBreak
    ) {
        return spansWithAddedBlank(
            state,
            timestamp,
            timestamp - state.targetStartTime,
            state.target
        );
    } else {
        return state.spans;
    }
}
function pauseTimerState(state, timestamp) {
    return state.running
        ? {
            running: false,
            pausedTimeCompleted: timestamp - state.startTime,
        }
        : {};
}
function startTimerState(state, timestamp, reset = true) {
    return {
        running: true,
        alarming: false,
        startTime: reset ? timestamp - state.pausedTimeCompleted : state.startTime,
        lastChanged: timestamp,
        lastTimerChange: timestamp,
    };
}
function filterObjectByKeys(object, keys) {
    keys = new Set(keys);
    return Object.fromEntries(
        Object.entries(object).filter(([k]) => keys.has(k))
    );
}
function ensureWorkStage(state) {
    if (!state.stages[state.currentStage].isBreak) return {};
    for (let i = 1; i < state.stages.length; i++) {
        if (!state.stages[(state.currentStage + i) % state.stages.length].isBreak)
            return { currentStage: state.currentStage + 1 };
    }
    return {};
}
function resetTimerState(state, timestamp) {
    return {
        lastChanged: timestamp,
        running: false,
        pausedTimeCompleted: 0,
        startTime: 0,
        lastTimerChange: timestamp,
    };
}
function setTimerTargetState(state, { target, startTimer, time }) {
    if (state.target.id === target.id) {
        if (!state.running && startTimer) {
            return {
                ...state,
                ...startTimerState(state, time),
                ...ensureWorkStage(state),
            };
        } else {
            return { ...state, target: target };
        }
    } else {
        const updatedSpans = spansWithAdded(state, time);
        return {
            ...state,
            spans: updatedSpans,
            target: target,
            targetStartTime: time,
            ...(startTimer && startTimerState(state, time, !state.running)),
            ...(startTimer && ensureWorkStage(state)),
            lastChanged: updatedSpans !== state.spans ? time : state.lastChanged,
        };
    }
}

/* Action Types */
const START_TIMER = "focustask/timer/START_TIMER";
const PAUSE_TIMER = "focustask/timer/PAUSE_TIMER";
const RESET_TIMER = "focustask/timer/RESET_TIMER";
const SET_TIMER_TARGET = "focustask/timer/SET_TIMER_TARGET";
const SET_TIMER_STAGES = "focustask/timer/SET_TIMER_STAGES";
const SET_TIMER_CURRENT_STAGE = "focustask/timer/SET_TIMER_CURRENT_STAGE";
const SET_TIMER_RUNNING = "focustask/timer/SET_TIMER_RUNNING";
const SET_TIMER_ALARMING = "focustask/timer/SET_TIMER_ALARMING";
const PUT_SPANS = "focustask/timer/PUT_SPANS";
const COMPLETE_SAVE = "focustask/timer/COMPLETE_SAVE";
const DELETE_SPAN = "focustask/timer/DELETE_SPAN";
const UPDATE_SPAN = "focustask/timer/UPDATE_SPAN";
const ADD_EMPTY_SPAN = "focustask/timer/ADD_EMPTY_SPAN";
const RESTORE_TIMER_STATE = "focustask/timer/RESTORE_TIMER_STATE";

/* Reducer */
export default function (state, action) {
    const defaultStages = [
        { time: 25 * 60 },
        { time: 5 * 60, isBreak: true },
        { time: 25 * 60 },
        { time: 5 * 60, isBreak: true },
        { time: 25 * 60 },
        { time: 5 * 60, isBreak: true },
        { time: 25 * 60 },
        { time: 15 * 60, isBreak: true },
    ];
    state = {
        target: {},
        stages: defaultStages,
        currentStage: 0,
        targetStartTime: 0,
        pausedTimeCompleted: 0,
        startTime: 0,
        running: false,
        spans: {},
        lastSaved: 0,
        lastLoad: 0,
        lastChanged: 0,
        lastTimerChange: 0,
        ...state,
        currentStage: (state && state.currentStage) ? (state.currentStage < (state.stages || defaultStages).length ? state.currentStage : 0) : 0,
    };
    switch (action.type) {
        case START_TIMER:
            return {
                ...state,
                ...startTimerState(state, action.time),
            };
        case PAUSE_TIMER:
            return {
                ...state,
                ...pauseTimerState(state, action.time),
                lastChanged: action.time,
                lastTimerChange: action.time,
            };
        case RESET_TIMER:
            return {
                ...state,
                ...resetTimerState(state, action.time),
            };
        case SET_TIMER_TARGET:
            return { ...state, ...setTimerTargetState(state, action) };
        case SET_TIMER_CURRENT_STAGE:
            return {
                ...state,
                currentStage: action.stage,
                lastChanged: state.running ? action.time : state.lastTimerChange,
                lastTimerChange: state.running ? action.time : state.lastTimerChange,
            };
        case SET_TIMER_STAGES:
            return { ...state, stages: action.stages };
        case SET_TIMER_RUNNING:
            if (!action.running) {
                return {
                    ...state,
                    running: false,
                    ...resetTimerState(state, action.time),
                    currentStage: (state.currentStage + 1) % state.stages.length,
                    pausedTimeCompleted: 0,
                    spans: spansWithAdded(state, action.time),
                    lastChanged: action.time,
                    lastTimerChange: action.time,
                };
            }
            return { ...state, ...startTimerState(state, action.time) };
        case SET_TIMER_ALARMING:
            if (action.alarming) {
                return {
                    ...state,
                    alarming: true,
                };
            } else {
                return { ...state, alarmgin: action.alarming };
            }
        case COMPLETE_SAVE:
            return {
                ...state,
                lastSaved: action.time,
                spans: Object.fromEntries(
                    Object.entries(state.spans).map(([k, v]) => [
                        k,
                        { ...v, saved: action.time },
                    ])
                ),
            };
        case DELETE_SPAN:
            return {
                ...state,
                lastChanged: action.time,
                spans: Object.fromEntries(
                    Object.entries(state.spans).map(([k, v]) =>
                        v.id.toString() === action.id.toString()
                            ? [k, { ...v, deleted: true, updated: action.time }]
                            : [k, v]
                    )
                ),
            };
        case UPDATE_SPAN:
            return {
                ...state,
                lastChanged: action.time,
                spans: Object.fromEntries(
                    Object.entries(state.spans).map(([id, previousSpan]) => {
                        if (id.toString() === action.span.id.toString()) {
                            if (
                                action.span.start !== previousSpan.start ||
                                previousSpan.start
                            ) {
                                action.span.duration = action.span.end - action.span.start;
                                delete action.span["start"];
                            }
                            return [id, { ...action.span, updated: action.time }];
                        } else {
                            return [id, previousSpan];
                        }
                    })
                ),
            };
        case ADD_EMPTY_SPAN:
            return {
                ...state,
                lastChanged: action.time,
                spans: spansWithAddedBlank(state, action.time, 300000, action.task),
            };
        case PUT_SPANS:
            const newSpans = Object.fromEntries(
                Object.entries(action.spans).filter(
                    ([k, v]) =>
                        !(k in state.spans) ||
                        (state.spans[k].updated || 0) < (v.updated || 0)
                )
            );
            return {
                ...state,
                lastLoad: action.time,
                lastChanged: Object.values(newSpans).length
                    ? state.lastChanged
                    : action.time,
                spans: {
                    ...Object.fromEntries(
                        Object.entries(state.spans).filter(
                            ([k, v]) =>
                                action.spans === false || k in action.spans || !v.saved
                        )
                    ),
                    ...newSpans,
                },
            };
        case RESTORE_TIMER_STATE:
            return {
                ...state,
                ...(action.timerState.lastTimerChange > state.lastTimerChange &&
                    filterObjectByKeys(action.timerState, [
                        "running",
                        "pausedTimeCompleted",
                        "target",
                        "startTime",
                        "currentStage",
                        "lastTimerChange",
                    ])),
            };
        case taskActions.CREATE_TASK:
            return action.startTimer
                ? {
                    ...state,
                    ...setTimerTargetState(state, {
                        target: action.task,
                        startTimer: true,
                        time: action.time,
                    }),
                }
                : state;
    }
    return state;
}

/* Action creators */
export function startTimer() {
    return { type: START_TIMER, time: new Date().getTime() };
}
export function pauseTimer() {
    return { type: PAUSE_TIMER, time: new Date().getTime() };
}
export function setTimerTarget(target, startTimer = true) {
    return {
        type: SET_TIMER_TARGET,
        target,
        time: new Date().getTime(),
        startTimer,
    };
}
export function setTimerCurrentStage(stage) {
    return { type: SET_TIMER_CURRENT_STAGE, stage, time: new Date().getTime() };
}
export function setTimerStages(stages) {
    return { type: SET_TIMER_STAGES, stages };
}
export function setTimerRunning(running) {
    return { type: SET_TIMER_RUNNING, running, time: new Date().getTime() };
}
export function setAlarming(alarming) {
    return { type: SET_TIMER_ALARMING, alarming };
}
export function resetTimer() {
    return { type: RESET_TIMER, time: new Date().getTime() };
}
export function putSpans(spans) {
    return { type: PUT_SPANS, spans, time: new Date().getTime() };
}
export function completeSave() {
    return { type: COMPLETE_SAVE, time: new Date().getTime() };
}
export function deleteSpan(id) {
    return { type: DELETE_SPAN, id, time: new Date().getTime() };
}
export function updateSpan(span) {
    return { type: UPDATE_SPAN, span, time: new Date().getTime() };
}
export function addEmptySpan(task) {
    return { type: ADD_EMPTY_SPAN, task, time: new Date().getTime() };
}
export function restoreTimerState(timerState) {
    return { type: RESTORE_TIMER_STATE, timerState };
}
