import { combineReducers } from 'redux';
import tasks from './tasks';
import app from './app';
import timer from './timer';
import filepicker from './filepicker';

export default combineReducers({
  tasks,
  app,
  timer,
  filepicker,
})