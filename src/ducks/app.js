import * as timeUtil from "../util/time";

/* Action Types */
const SET_NOTIFICATIONS_AVAILABLE = "focustask/app/SET_NOTIFICATIONS_AVAILABLE";
const SET_LOGGED_IN_USERNAME = "focustask/app/SET_LOGGED_IN_USERNAME";
const SET_ERROR = "focustask/app/SET_ERROR";
const SET_ONLINE_STATUS = "focustask/app/SET_ONLINE_STATUS";
const SET_THEME = "focustask/app/SET_THEME";
const SET_DATE_RANGE_START = "focustask/app/SET_DATE_RANGE_START";
const SET_DATE_RANGE_END = "focustask/app/SET_DATE_RANGE_END";

/* Reducer */
export default function (state, action) {
  state = {
    notificationsAvailable: false,
    loggedInUser: {},
    error: false,
    online: navigator.onLine,
    theme: "dark2",
    dateRange: {
      start: timeUtil
        .fromParts({
          ...timeUtil.parts(new Date()),
          day: new Date().getDate() - 7 - new Date().getDay(),
        })
        .getTime(),
      end: new Date().getTime(),
    },
    ...state,
  };
  switch (action.type) {
    case SET_NOTIFICATIONS_AVAILABLE:
      return {
        ...state,
        notificationsAvailable: action.notificationsAvailable,
      };
    case SET_LOGGED_IN_USERNAME:
      return {
        ...state,
        loggedInUser: { ...state.loggedInUser, username: action.username },
      };
    case SET_ERROR:
      return { ...state, error: action.error };
    case SET_ONLINE_STATUS:
      return { ...state, online: action.online };
    case SET_THEME:
      return { ...state, theme: action.theme };
    case SET_DATE_RANGE_END:
      return { ...state, dateRange: { ...state.dateRange, end: action.date } };
    case SET_DATE_RANGE_START:
      return {
        ...state,
        dateRange: { ...state.dateRange, start: action.date },
      };
  }
  return state;
}

/* Action creators */
export function setNotificationsAvailable(areAvailable) {
  return {
    type: SET_NOTIFICATIONS_AVAILABLE,
    notificationsAvailable: areAvailable,
  };
}
export function setLoggedInUserName(username) {
  return { type: SET_LOGGED_IN_USERNAME, username };
}
export function setError(error) {
  return { type: SET_ERROR, error };
}
export function triggerError(errorMessage) {
  return (dispatch) => {
    dispatch(setError(errorMessage));
    window.setTimeout(() => dispatch(setError(false)), 10000);
  };
}
export function setOnlineStatus(online) {
  return { type: SET_ONLINE_STATUS, online };
}
export function setTheme(theme) {
  return { type: SET_THEME, theme };
}
export function setDateRangeStart(date) {
  return { type: SET_DATE_RANGE_START, date: date.getTime() };
}
export function setDateRangeEnd(date) {
  return { type: SET_DATE_RANGE_END, date: date.getTime() };
}
