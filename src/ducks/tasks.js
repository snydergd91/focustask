import * as timeUtil from "../util/time";
import dateFormat from "dateformat";

function createId() {
    return `${new Date().getTime()}${Math.floor(Math.random() * 1000)}`;
}
export const util = {
    taskToLine(task) {
        return `${task.title}`;
    },
    parseTask(line) {
        const customLabels = Object.fromEntries(
            [...line.matchAll(/(?:^|\s)([a-zA-Z]\S*?):(\S+)/g)].map((x) => [
                x[1],
                x[2],
            ])
        );
        const title = line
            .replace(/(^|\s+)id:\S+/g, "")
            .replace(/(^|\s+)updated:\S+/g, "");
        const contextTags = [...line.matchAll(/(?:^|\s+)@(\w+)/g)].map((x) => x[1]);
        const projectTags = [...line.matchAll(/(?:^|\s+)\+(\w+)/g)].map(
            (x) => x[1]
        );
        const complete = line.startsWith("x ");

        return {
            customLabels,
            title,
            contextTags,
            projectTags,
            complete,
            ...(customLabels.updated && { updated: parseInt(customLabels.updated) }),
            id: customLabels.id || createId(),
        };
    },
    createTasksFile(tasks) {
        const stringTasks = [];
        const orderedTasks = Object.values(tasks);
        orderedTasks.sort((a, b) => b.order - a.order);
        for (let task of Object.values(tasks)) {
            stringTasks.push(util.taskToLine(task));
        }
        return stringTasks.join("\n");
    },
    getConsistentlyUpdatedTask(previous, next, updateTime) {
        if (previous.title != next.title) {
            return {
                ...next,
                ...util.parseTask(next.title),
                id: next.id,
                updated: updateTime,
                deleted: next.title.length === 0,
            };
        } else {
            const myReturn = { ...next, updated: updateTime };
            if (previous.complete != next.complete) {
                if (next.complete) {
                    myReturn.title = "x " + myReturn.title;
                } else {
                    myReturn.title = myReturn.title.replace(/^x /, "");
                }
            }
            return myReturn;
        }
    },
    sortOptions: {
        "last-recorded": {
            title: "Last Recorded",
            taskKeyLister: (tasks, spans) => {
                const lastRecordedTimes = {};
                for (const span of spans) {
                    if (!(span.taskId in lastRecordedTimes)) {
                        lastRecordedTimes[span.taskId] = span.end - span.duration;
                    } else {
                        lastRecordedTimes[span.taskId] = Math.max(span.end - span.duration, lastRecordedTimes[span.taskId])
                    }
                }

                const sortedKeys = Object.keys(tasks);
                sortedKeys.sort((a, b) => (tasks[a].complete ? 1 : 0) - (tasks[b].complete ? 1 : 0) ||
                    lastRecordedTimes[tasks[b].id] - lastRecordedTimes[tasks[a].id]);
                return sortedKeys;
            }
        },
        "last-recorded-grouped": {
            title: "Last Recorded (grouped)",
            taskKeyLister: (tasks, spans, dateRange) => {
                const days = {};
                const notUsedRecently = new Set(Object.keys(tasks));
                for (const span of spans) {
                    const date = timeUtil.extractDate(new Date(span.end - span.duration));
                    const dayKey = date.getTime();
                    if (span.taskId in tasks && dayKey <= dateRange.end && dayKey >= dateRange.start) {
                        if (notUsedRecently.has(span.taskId)) {
                            notUsedRecently.delete(span.taskId);
                        }
                        if (!(dayKey in days)) {
                            days[dayKey] = {};
                        }
                        if (span.taskId in days[dayKey]) {
                            days[dayKey][span.taskId] = Math.max(days[dayKey][span.taskId], span.end - span.duration)
                        } else {
                            days[dayKey][span.taskId] = span.end - span.duration;
                        }
                    }
                }
                for (const dayKey in days) {
                    days[dayKey] = [...Object.keys(days[dayKey])].sort((a, b) => days[dayKey][b] - days[dayKey][a])
                }
                const byDate = Object.entries(days).sort(([k1], [k2]) => k2 - k1).map((x) => [dateFormat(new Date(parseInt(x[0])), "dddd, mmmm d"), x[1]]);
                if (notUsedRecently.size > 0) {
                    byDate.push(["Not used recently", [...notUsedRecently]]);
                }
                return byDate;
            }
        },
        "manual": {
            title: "Manual",
            taskKeyLister: (tasks, spans) => {
                return Object.keys(tasks).sort((a, b) => tasks[b].order - tasks[a].order);
            }
        }
    },
};

/* Action Types */
const PUT_TASKS = "focustask/tasks/PUT_TASKS";
const SET_LOADING = "focustask/tasks/SET_LOADING";
const COMPLETE_TASK = "focustask/tasks/DELETE_TASK";
const UPDATE_TASK = "focustask/tasks/UPDATE_TASK";
const REORDER_TASK = "focustask/tasks/REORDER_TASK";
export const CREATE_TASK = "focustask/tasks/CREATE_TASK";
const COMPLETE_SAVE = "focustask/tasks/COMPLETE_SAVE";
const CHANGE_SORT = "focustask/tasks/CHANGE_SORT";

/* Reducer */
export default function (
    state,
    action
) {
    state = {
        tasks: {},
        loading: true,
        lastSaved: 0,
        lastChanged: 0,
        lastLoad: 0,
        sort: 'manual',
        ...state,
    }
    switch (action.type) {
        case PUT_TASKS:
            const newTasks = Object.fromEntries(
                Object.entries(action.tasks).filter(
                    (x) =>
                        !(x[0] in state.tasks) ||
                        (state.tasks[x[0]].updated || 0) < (x[1].updated || 0)
                )
            );
            return {
                ...state,
                lastLoad: action.time,
                lastChanged: Object.values(newTasks).length
                    ? state.lastChanged
                    : action.time,
                tasks: {
                    ...Object.fromEntries(
                        Object.entries(state.tasks).filter(
                            ([k, v]) =>
                                action.tasks === false || k in action.tasks || !v.saved
                        )
                    ),
                    ...newTasks,
                },
                loading: false,
            };
        case SET_LOADING:
            return { ...state, loading: action.loading };
        case COMPLETE_TASK:
            if (state.tasks[action.task.id].complete) {
                return state;
            } else {
                return {
                    ...state,
                    tasks: {
                        ...state.tasks,
                        [action.task.id]: {
                            ...state.tasks[action.task.id],
                            complete: true,
                            title: "x " + state.tasks[action.task.id].title,
                            updated: action.time,
                        },
                    },
                    lastChanged: action.time,
                };
            }
        case UPDATE_TASK:
            return {
                ...state,
                tasks: Object.fromEntries(
                    Object.entries(state.tasks).map((x) =>
                        x[0] === action.updated.id
                            ? [
                                x[0],
                                util.getConsistentlyUpdatedTask(
                                    state.tasks[action.updated.id],
                                    action.updated,
                                    action.time
                                ),
                            ]
                            : x
                    )
                ),
                lastChanged: action.time,
            };
        case REORDER_TASK:
            return {
                ...state,
                lastChanged: action.time,
                tasks: (function () {
                    if (action.movement == 0) return state.tasks;

                    // make sure tasks all have an order and get a list of them sorted by order
                    let newTasks = Object.values(state.tasks);
                    let nextOrder =
                        newTasks.reduce((a, b) => Math.max(b.order || 0, a), 0) + 1;
                    newTasks = newTasks.map((x) =>
                        "order" in x ? x : { ...x, order: nextOrder++ }
                    );
                    newTasks.sort((a, b) => a.order - b.order);

                    // move specified task by "movement" amount
                    const index = newTasks
                        .map((v, i) => [i, v])
                        .filter(([i, v]) => v.id === action.task.id)[0][0];
                    const otherIndex = Math.max(
                        Math.min(index + action.movement, newTasks.length - 1),
                        0
                    );
                    newTasks[index] = {
                        ...newTasks[index],
                        order: newTasks[otherIndex].order,
                        updated: action.time,
                    };

                    // move displaced tasks over by 1
                    for (let i = otherIndex; i != index; i -= action.movement) {
                        newTasks[i] = {
                            ...newTasks[i],
                            order: newTasks[i].order + (action.movement > 0 ? -1 : 1),
                            updated: action.time,
                        };
                    }
                    return Object.fromEntries(newTasks.map((x) => [x.id, x]));
                })(),
            };
        case CREATE_TASK: {
            return {
                ...state,
                lastChanged: action.time,
                tasks: {
                    ...state.tasks,
                    [action.task.id]: {
                        ...action.task,
                        order:
                            Object.values(state.tasks).reduce(
                                (a, b) => Math.max(b.order || 0, a),
                                0
                            ) + 1,
                    },
                },
            };
        }
        case COMPLETE_SAVE:
            return {
                ...state,
                lastSaved: action.time,
                tasks: Object.fromEntries(
                    Object.entries(state.tasks).map(([k, v]) => [
                        k,
                        { ...v, saved: action.time },
                    ])
                ),
            };
        case CHANGE_SORT:
            return {
                ...state,
                sort: action.order,
            };
    }
    return state;
}

/* Action creators */
export function putTasks(tasks) {
    return { type: PUT_TASKS, tasks, time: new Date().getTime() };
}
export function setLoading(loading) {
    return { type: SET_LOADING, loading };
}
export function createTask(name, startTimer) {
    const time = new Date().getTime();
    return {
        type: CREATE_TASK,
        task: {
            title: name,
            ...util.parseTask(name),
            id: createId(),
            updated: time,
        },
        time,
        startTimer,
    };
}
export function completeTask(task) {
    return { type: COMPLETE_TASK, task, time: new Date().getTime() };
}
export function updateTask(updated) {
    return { type: UPDATE_TASK, updated, time: new Date().getTime() };
}
export function reorderTask(task, movement) {
    return { type: REORDER_TASK, task, movement, time: new Date().getTime() };
}
export function completeSave() {
    return { type: COMPLETE_SAVE, time: new Date().getTime() };
}
export function setSort(order) {
    return { type: CHANGE_SORT, order };
}